var oReq = new XMLHttpRequest();
var filename = "images/XBox69px.png";
oReq.open("GET",filename, true);
oReq.responseType = "arraybuffer";
var arrayBuffer;
var binaryString;
var img = new Image();
var hexColorMap;
var alphaMap;
var svgString = '';

oReq.onload = function (oEvent) {

    arrayBuffer = oReq.response; // Note: not oReq.responseText
    binaryString = '';

    if (arrayBuffer) {

        var byteArray = new Uint8Array(arrayBuffer);

        for (var i = 0; i < byteArray.byteLength; i++) {

            binaryString += String.fromCharCode(byteArray [i]); //extracting the bytes

        }

        var base64 = window.btoa(binaryString); //creating base64 string


        img.src = "data:image/png;base64," + base64; //creating a base64 uri
        hexColorMap = new Array(img.width);
        alphaMap = new Array(img.width);
        for(var i=0; i<hexColorMap.length;i+=1){
            hexColorMap[i] = new Array(img.height);
            alphaMap[i] = new Array(img.height);
        }

    }
};

oReq.send(null);
img.onload = function () {
    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;

    var context = canvas.getContext('2d');
    context.drawImage(img, 0, 0);

    var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

    for(var y = 0; y < img.height; y += 1){
        for(var x = 0; x < img.width; x += 1){
            // Now you can access pixel data from imageData.data.
            // It's a one-dimensional array of RGBA values.
            // Here's an example of how to get a pixel's color at (x,y)
            var index = (y * imageData.width + x) * 4;
            var red = imageData.data[index];
            var green = imageData.data[index + 1];
            var blue = imageData.data[index + 2];
            var alpha = imageData.data[index + 3];
            hexColorMap[x][y] = rgbToHex(red, green, blue, alpha);
            alphaMap[x][y] = ((alpha/255).toFixed(2));
            //console.log(alpha+'->'+alphaMap[x][y],',');
        }
    }

    svgString = toSvg(hexColorMap);
    document.write(svgString+"<br/><a name='"+filename+"' href='data:image/svg+xml;utf8,"+svgString+"'>"+filename+"</a>");

};

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b, a) {
    if(a === 0){
        return -1;
    }else {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
}

function toSvg(colorMap){
    var stringData;
    var widthVal = colorMap.length;
    var heightVal = colorMap[0].length
    var alphaVal = 1;
    stringData = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
    stringData += '<svg xmlns="http://www.w3.org/2000/svg" width="'+widthVal+'" height="'+heightVal+'" viewBox="0 0 '+widthVal+' '+heightVal+'" shape-rendering="crispEdges">';
    for(var x = 0; x < colorMap.length; x+=1){
        for(var y = 0; y < colorMap[x].length; y+=1){
            var currentColor = colorMap[x][y];
            var currentAlpha = alphaMap[x][y];
            var x2 = x+1;
            var barWidth = 1;
            var opacityChange = '';
            while(currentColor !== -1 && x2 < colorMap.length && colorMap[x2][y] === currentColor && alphaMap[x2][y] === currentAlpha){
                barWidth += 1;
                colorMap[x2][y] = -2;
                x2 += 1;
            }
            if(colorMap[x][y] !== -1 && colorMap[x][y] !== -2){
                alphaVal = alphaMap[x][y];
                if(alphaVal !== "1.00"){
                    opacityChange = 'fill-opacity="'+alphaVal+'"';
                };

                //stringData += '<rect width="' + barWidth + '" height="1" x="' + x + '" y="' + y + '" style="fill:' + colorMap[x][y] + '' + opacityChange + '"></rect>';
                stringData += '<rect width="'+barWidth+'" height="1" x="' + x + '" y="' + y + '" fill="' + colorMap[x][y] + '" '+opacityChange+'></rect>';
            }
        }
    }
    stringData += '</svg>';

    return stringData;

}